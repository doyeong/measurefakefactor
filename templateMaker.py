import DefineControlRegions
import numpy
import os, sys, re
import array
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2F
from ROOT import gROOT

gROOT.Macro("pluginFakeFactor/get_raw_FF.C+")

def buildFakeFactors(nCat): #jetMultiplicity):
    print "\t\033[1;37;44mEstimate fakes using my fake factor...\33[0m"
    # Compute Fakes
    # (corr_ff*(0.5*frac_real + frac_w + frac_tt)) where (0.5*frac_real + frac_w + frac_tt) = fractionWeight
    zeroJet = "(njets==0)"
    multiJet = "(njets>0)"
    corrFakeFactorZero_1 = "(getRawZeroJetFF(%s)*getClosurCorrZero(vis_mass)*getLepIsoCorrZero(t2_pt)*getOSextrapolCorrZero(vis_mass))"%("t1_pt")
    corrFakeFactorZero_2 = "(getRawZeroJetFF(%s)*getClosurCorrZero(vis_mass)*getLepIsoCorrZero(t2_pt)*getOSextrapolCorrZero(vis_mass))"%("t2_pt")
    corrFakeFactorMulti_1 = "(getRawMultiJetFF(%s)*getClosurCorrMulti(vis_mass)*getLepIsoCorrMulti(t2_pt)*getOSextrapolCorrMulti(vis_mass))"%("t1_pt")
    corrFakeFactorMulti_2 = "(getRawMultiJetFF(%s)*getClosurCorrMulti(vis_mass)*getLepIsoCorrMulti(t2_pt)*getOSextrapolCorrMulti(vis_mass))"%("t2_pt")
    # Jet multiplicity binned
    corrFakeFactor_1 = "((%s*%s)+(%s*%s))" % (zeroJet, corrFakeFactorZero_1, multiJet, corrFakeFactorMulti_1)
    corrFakeFactor_2 = "((%s*%s)+(%s*%s))" % (zeroJet, corrFakeFactorZero_2, multiJet, corrFakeFactorMulti_2)

    fractionWeight_1 = "((%s)+(%s)+(%s))" % ("getFractionFakes(vis_mass, njets, 1, "+nCat+")","getFractionW(vis_mass, njets, 1, "+nCat+")","getFractionTT(vis_mass, njets, 1, "+nCat+")")
    fractionWeight_2 = "((%s)+(%s)+(%s))" % ("getFractionFakes(vis_mass, njets, 2, "+nCat+")","getFractionW(vis_mass, njets, 2, "+nCat+")","getFractionTT(vis_mass, njets, 2, "+nCat+")")

    weight_1 = "(%s*%s)" % (corrFakeFactor_1,fractionWeight_1)
    weight_2 = "(%s*%s)" % (corrFakeFactor_2,fractionWeight_2)
    return weight_1, weight_2

    
def storeFinBinningHistogram(path, file_list, TauWP, outputTDir, njets, parameter, bins, charge, category, whichFakes):
    isoRegion, aiRegion1, aiRegion2 = DefineControlRegions.derivingRegion(TauWP, charge, njets)
    catZeroJet, catVBF, catBoosted = DefineControlRegions.categories("mjj300")#HIG_16_043")    

    print "\tVariables :\t" + parameter
    print "\tBinnings :\t" + bins

    weight = "(%s*%s)" % (isoRegion, "evtwt")
    nCat = "3.0"
    if "0jet" in category:
        weight = "(%s*%s*%s)" % (isoRegion, "evtwt", catZeroJet)
        nCat = "0.0"
    elif "boosted" in category:
        weight = "(%s*%s*%s)" % (isoRegion, "evtwt", catBoosted)
        nCat = "1.0"
    elif "vbf" in category:
        weight = "(%s*%s*%s)" % (isoRegion, "evtwt", catVBF)
        nCat = "2.0"

    dimension = len(bins.split(','))/3.0

    # Loop over all inputfiles
    for inputFile in file_list:
        f=ROOT.TFile(path+"/"+inputFile)
        tree = f.Get("tautau_tree")  
        processName = (inputFile.split('.'))[0]
        # Use MC
        #if "emb" in processName:
        #    continue

        print "\tExtracting histograms as a function of " +parameter+ " ... " + processName 
        #print parameter+">>"+processName+"("+bins+")", weight
        if dimension==2:
            if file_list[0]==inputFile:
                print "\t2D histograms are storing ... "
            tree.Draw(parameter+">>"+processName+"("+bins+")", weight, "colz")        
        else:
            if file_list[0]==inputFile:
                print "\t1D histograms are storing ... "
            tree.Draw(parameter+">>"+processName+"("+bins+")", weight)        
        if dimension>2:
            # Extract 3rd variable info
            var1, var2, var3 = (parameter.split(':'))[0], (parameter.split(':'))[1], (parameter.split(':'))[2]
            nBin1, minBin1, maxBin1 = float((bins.split(','))[0]), float((bins.split(','))[1]), float((bins.split(','))[2])
            nBin2, minBin2, maxBin2 = float((bins.split(','))[3]), float((bins.split(','))[4]), float((bins.split(','))[5])
            nBin3, minBin3, maxBin3 = float((bins.split(','))[6]), float((bins.split(','))[7]), float((bins.split(','))[8])
            step1, step2, step3 = (maxBin1-minBin1)/nBin1, (maxBin2-minBin2)/nBin2, (maxBin3-minBin3)/nBin3
            binning1 = numpy.array(numpy.arange(minBin1, maxBin1+step1, step1))
            binning2 = numpy.array(numpy.arange(minBin2, maxBin2+step2, step2))
            binning3 = numpy.array(numpy.arange(minBin3, maxBin3+step3, step3))
            # Extract 1st and 2nd variable info
            var = "%s:%s" % (var2, var1) # Bug in ROOT? order is opposite when add >>
            bin = ""
            for idx in  range(6):
                bin += bins.split(',')[idx] + ","
            bin = bin[0:-1]

            bin1D = ""
            for idx in range(3):
                bin1D += bins.split(',')[idx] + ","
            bin1D = bin1D[0:-1]
            
            if file_list[0]==inputFile:
                print "\t3rd variables %s has %s bins in [%s, %s]" % (var3 ,str(nBin3), str(minBin3), str(maxBin3))                
                print "\tstep : %s, binning : %s " % (step3,binning3)
                print "\t[Draw info] var : %s, bin : %s\n" % (var, bin)
            for binEdge3 in binning3:
                addCut3 = "((%s>%s) && (%s<%s))" % (var3, str(binEdge3), var3, str(binEdge3+step3))
                weightWithAddCut3 = "(%s*%s)" % (addCut3, weight)
                postFixHist_var3 = "_%s%sto%s" % (var3,str(binEdge3),str(binEdge3+step3))
                #print addCut
                tree.Draw(var+">>"+processName+postFixHist_var3+"("+bin+")", weightWithAddCut3, "colz")

                histo2D = f.Get(processName+postFixHist_var3)
                histo2D.SetName(processName+postFixHist_var3)
                outputTDir.cd()
                histo2D.Write()
                f.cd()

                for binEdge2 in binning2:
                    addCut2 = "((%s>%s) && (%s<%s))" % (var2, str(binEdge2), var2, str(binEdge2+step2))
                    weightWithAddCut2 = "(%s*%s)" % (addCut2, weightWithAddCut3)
                    postFixHist_var2 = "_%s%sto%s" % (var2,str(binEdge2),str(binEdge2+step2))
                    tree.Draw(var1+">>"+processName+postFixHist_var3+postFixHist_var2+"("+bin1D+")", weightWithAddCut2)
                    histo1D = f.Get(processName+postFixHist_var3+postFixHist_var2)
                    histo1D.SetName(processName+postFixHist_var3+postFixHist_var2)
                    outputTDir.cd()
                    histo1D.Write()
                    f.cd()

        histo = f.Get(processName)
        histo.SetName(processName)


        # Estimate fakes with data
        if "data" in processName:
            if "my" in whichFakes:
                weight_1, weight_2 = buildFakeFactors(nCat)
                ffWeight = "(0.5*(%s*%s)+0.5*(%s*%s))" % (weight_1,aiRegion1,weight_2,aiRegion2)
                '''
                print "\t>> weight_1 : " +  weight_1
                print "\t>> weight_2 : " +  weight_2
                print "\t>> ffWeight : " + ffWeight
                '''
                tree.Draw(parameter+">>jetFakes("+bins+")", ffWeight)
                #tree.Draw(parameter+":"+ffWeight">>vsffWeight")
                histoFakes = f.Get("jetFakes")
                histoFakes.SetName("jetFakes")
                #histoFakesW = f.Get("vsffWeight")
                #histoFakesW.SetName("vsffWeight")
                outputTDir.cd()
                histoFakes.Write()
                #histoFakesW.Write()
            else:
                nbins = (bins.split(','))[0]
                minBin = (bins.split(','))[1]
                maxBin = (bins.split(','))[2]
                binning = "%s %s %s %s" % (nbins,minBin,maxBin,parameter)
                cmd =  "\t033[1;94m./plotter_tt.exe %s/data_obs.root jetFakes.root jetFakes %s 0000 2018 noAC\33[0m" % (path,binning)
                print cmd
                #os.system(cmd)
        outputTDir.cd()
        histo.Write()

        f.Close()
