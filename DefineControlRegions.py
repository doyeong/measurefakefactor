# CUTS
zeroJet = "(njets==0)"
multiJet = "(njets>0)"
VBF_HIG_16_043 = "(njets>1 && dEtajj>2.5 && higgs_pT>100)"
VBF_MJJ300 = "(njets>1 && mjj>300)"
VBF_MJJ350 = "(njets>1 && mjj>350)"

chargeSS = "(t1_charge*t2_charge>0)" # DR
chargeOS = "(t1_charge*t2_charge<0)" # SR, AR

MVATight_IR = "(t1_iso_T && t2_iso_T)"
MVATight_AR1 = "(t2_iso_T && !t1_iso_T && t1_iso_VL)" # AI t1, Iso t2
MVATight_AR2 = "(t1_iso_T && !t2_iso_T && t2_iso_VL)" # AI t2, Iso t1

DeepVTight_IR = "(t1_DNNisoJ_VT && t2_DNNisoJ_VT)"
DeepVTight_AR1 = "(t2_DNNisoJ_VT && !t1_DNNisoJ_VT && t1_DNNisoJ_VVVL)"
DeepVTight_AR2 = "(t1_DNNisoJ_VT && !t2_DNNisoJ_VT && t2_DNNisoJ_VVVL)"

DeepTight_IR = "(t1_DNNisoJ_T && t2_DNNisoJ_T)"
DeepTight_AR1 = "(t2_DNNisoJ_T && !t1_DNNisoJ_T && t1_DNNisoJ_VVVL)"
DeepTight_AR2 = "(t1_DNNisoJ_T && !t2_DNNisoJ_T && t2_DNNisoJ_VVVL)"

DeepMedium_IR = "(t1_DNNisoJ_M && t2_DNNisoJ_M)"
DeepMedium_AR1 = "(t2_DNNisoJ_M && !t1_DNNisoJ_M && t1_DNNisoJ_VVVL)"
DeepMedium_AR2 = "(t1_DNNisoJ_M && !t2_DNNisoJ_M && t2_DNNisoJ_VVVL)"

DeepLoose_IR = "(t1_DNNisoJ_L && t2_DNNisoJ_L)"
DeepLoose_AR1 = "(t2_DNNisoJ_L && !t1_DNNisoJ_L && t1_DNNisoJ_VVVL)"
DeepLoose_AR2 = "(t1_DNNisoJ_L && !t2_DNNisoJ_L && t2_DNNisoJ_VVVL)"

DeepVLoose_IR = "(t1_DNNisoJ_VL && t2_DNNisoJ_VL)"
DeepVLoose_AR1 = "(t2_DNNisoJ_VL && !t1_DNNisoJ_VL && t1_DNNisoJ_VVVL)"
DeepVLoose_AR2 = "(t1_DNNisoJ_VL && !t2_DNNisoJ_VL && t2_DNNisoJ_VVVL)"

DeepTauEleMuRequirement_1 = "(t1_DNNisoM_VL && t1_DNNisoE_VL)"
DeepTauEleMuRequirement_2 = "(t2_DNNisoM_VL && t2_DNNisoE_VL)"

def derivingRegion(TauWP, chargeRequirment, njets):
    isoRegion = MVATight_IR
    ai1Region = MVATight_AR1
    ai2Region = MVATight_AR2
    if "Deep" in TauWP :
        if "VTight" in TauWP:
            DeepJetIsoRegion = DeepVTight_IR
            ai1Region = DeepVTight_AR1
            ai2Region = DeepVTight_AR2
        elif "VLoose" in TauWP:
            DeepJetIsoRegion = DeepVLoose_IR
            ai1Region = DeepVLoose_AR1
            ai2Region = DeepVLoose_AR2
        elif "Tight" in TauWP:
            DeepJetIsoRegion = DeepTight_IR
            ai1Region = DeepTight_AR1
            ai2Region = DeepTight_AR2
        elif "Loose" in TauWP:
            DeepJetIsoRegion = DeepLoose_IR
            ai1Region = DeepLoose_AR1
            ai2Region = DeepLoose_AR2
        elif "Medium" in TauWP:
            DeepJetIsoRegion = DeepMedium_IR
            ai1Region = DeepMedium_AR1
            ai2Region = DeepMedium_AR2
        isoRegion = "(%s && %s && %s)" % (DeepJetIsoRegion, DeepTauEleMuRequirement_1, DeepTauEleMuRequirement_2)

    OSorSS = "" 
    if chargeRequirment is "SS":
        OSorSS = chargeSS 
    elif chargeRequirment is "OS":
        OSorSS = chargeOS
    isoRegion = "(%s && %s)" % (isoRegion, OSorSS)
    ai1Region = "(%s && %s)" % (ai1Region, OSorSS)
    ai2Region = "(%s && %s)" % (ai2Region, OSorSS)

    njetSplitting = ""
    if njets is "zeroJet":
        njetSplitting = zeroJet
    elif njets is "multiJet":
        njetSplitting = multiJet
    if njetSplitting is not "":
        isoRegion = "(%s && %s)" % (isoRegion, njetSplitting)
        ai1Region = "(%s && %s)" % (ai1Region, njetSplitting)
        ai2Region = "(%s && %s)" % (ai2Region, njetSplitting)

    #print isoRegion
    #print ai1Region
    #print ai2Region
    return isoRegion, ai1Region, ai2Region


def categories(vbfDefinition):
    if "HIG" in vbfDefinition:
        Boosted = "(!(%s) && !(%s))" % (zeroJet, VBF_HIG_16_043)
        return zeroJet, VBF_HIG_16_043, Boosted
    elif "mjj300" in vbfDefinition:
        Boosted = "(!(%s) && !(%s))" % (zeroJet, VBF_MJJ300)
        return zeroJet, VBF_MJJ300, Boosted
    elif "mjj350" in vbfDefinition:
        Boosted = "(!(%s) && !(%s))" % (zeroJet, VBF_MJJ350)
        return zeroJet, VBF_MJJ350, Boosted
