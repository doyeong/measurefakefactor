#!/usr/bin/env python
import os, sys, re
from optparse import OptionParser
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2F
import templateMaker

parser = OptionParser()
parser.add_option('--raw', '-r', action='store_true',
                  default=False, dest='measurRawFF',
                  help='measure raw fakefactor'
                  )
parser.add_option('--var', '-v', action='store',
                  default="vis_mass", dest='variable',
                  help='observable which you plot'
                  )
parser.add_option('--bin', '-b', action='store',
                  default="50,0,300", dest='binning',
                  help='binning Nbins,min,max'
                  )
parser.add_option('--input', '-i', action='store',
                  default="/hdfs/store/user/doyeong/Sliced/Sliced_2018X10_forFF/nominal/merged/", dest='inputPath',
                  help='input root files folder for the plot'
                  )
(options, args) = parser.parse_args()

TauWP = "MVATight"#"DeepLoose"
ROOT.gROOT.SetBatch(ROOT.kTRUE)

def main():
    path=options.inputPath
    file_list = os.listdir(path)
    print ">> Start Plotter! "
    # Input/Output Files
    outputFileName = 'plotTemplate_Univ_'+options.variable+'.root'
    outputFile = TFile( outputFileName, 'recreate' )
    
    #categories = ['tt_inclusive', 'tt_vbf', 'tt_0jet', 'tt_boosted']
    categories = ['tt_vbf']
    for category in categories:
        print "\n\n\t\033[1;94;47m["+category+" is making]\33[0m"
        outputTDir = outputFile.mkdir(category)
        templateMaker.storeFinBinningHistogram(path, file_list, TauWP, outputTDir, "noNJetSplitting", options.variable, options.binning ,"OS", category, "officialFakes")

    outputFile.Close()

    print "\t\033[1;92m"+ outputFile.GetName() +" is produced :)\33[0m"
    print "\n\t\033[1;92mhadd -f h"+outputFileName+" "+outputFileName+" jetFakes.root"
    print "\t\033[1;92mpython python/stackPlotter_vbf.py -i h"+outputFileName+" -v "+options.variable+" -n 2018 -z --plotPowheg\33[0m"
    

if __name__ == "__main__":
    main()
