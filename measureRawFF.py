import DefineControlRegions
import os, sys, re
import array
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2F

def storeFinBinningHistogram(file_list, TauWP, outputTDir, njets, parameter, charge):
    isoDR, aiDR1, aiDR2 = DefineControlRegions.derivingRegion(TauWP, charge, njets)
    
    # Loop over all inputfiles
    for inputFile in file_list:
        f=ROOT.TFile("/hdfs/store/user/doyeong/Sliced/Sliced_2018X10_forFF/nominal/merged/"+inputFile)
        tree = f.Get("tautau_tree")  
        processName = (inputFile.split('.'))[0]
        # Use MC
        if "emb" in processName:
            continue

        print "\tExtracting histograms as a function of " +parameter+ " ... " + processName 
        extractBranch1 = parameter
        extractBranch2 = parameter
        fineBinning = "120,0,300"
        if parameter is "t2_pt":
            fineBinning = "160,40,120"
        elif parameter is "tauPT":
            extractBranch1 = "t1_pt"
            extractBranch2 = "t2_pt"
            fineBinning = "200,0,500"
            
        tree.Draw(extractBranch1+">>isoDR1("+fineBinning+")", isoDR)
        H_isoDR1 = f.Get("isoDR1")
        H_isoDR1.SetName("isoDR1_"+processName)
        
        tree.Draw(extractBranch1+">>aiDR1("+fineBinning+")", aiDR1)
        H_aiDR1 = f.Get("aiDR1")
        H_aiDR1.SetName("aiDR1_"+processName)
        
        tree.Draw(extractBranch2+">>isoDR2("+fineBinning+")", isoDR)
        H_isoDR2 = f.Get("isoDR2")
        H_isoDR2.SetName("isoDR2_"+processName)
        
        tree.Draw(extractBranch2+">>aiDR2("+fineBinning+")", aiDR2)
        H_aiDR2 = f.Get("aiDR2")
        H_aiDR2.SetName("aiDR2_"+processName)

        outputTDir.cd()
        H_isoDR1.Write()
        H_aiDR1.Write()
        H_isoDR2.Write()
        H_aiDR2.Write()
        
        f.Close()

# Merge Exp. histograms
dicMergedExpHisto = {}
typeOfHisto = ["isoDR1","aiDR1","isoDR2","aiDR2"]
def measureRawFF(outputFile, outputTDir_rawFakeFactor, inputTDir_fineBin):
    dicMergedExpHisto.clear()
    for keyTDir in outputFile.GetListOfKeys(): #Bad code but work due to historical reason
        dirName = keyTDir.GetName()
        if inputTDir_fineBin not in dirName:
            continue
        dir = outputFile.Get(dirName)
        dicMergedExpHisto[dirName]={}
        print "\n\n>> Merging Histos in WP "+dirName
        for keyHist in outputFile.Get(dirName).GetListOfKeys():
            histName = keyHist.GetName()
            hist = outputFile.Get(dirName+"/"+histName)        
        
            # Assumption : Obs. - all Exp. = QCD
            weight = -1
            if "data" in histName:
                continue # Always cloned with the before histogram
            
            typeHisto = (histName.split('_'))[0]
            # Add Exp. histograms depending on leg and region 
            if any(hType in typeHisto for hType in typeOfHisto):
                print "\tMerging histograms ... "+keyHist.GetName()+" weight:"+str(weight)
                if typeHisto not in dicMergedExpHisto.keys():
                    dicMergedExpHisto[dirName][typeHisto] = outputFile.Get(dirName+"/"+typeHisto+"_data_obs").Clone()
                dicMergedExpHisto[dirName][typeHisto].Add(hist.Clone(),weight)
        # Write expectd QCD in output root files
        outputTDir_rawFakeFactor.cd()
        rebinnedHist=[]
        del rebinnedHist[:]

        for typeH in typeOfHisto:
            dicMergedExpHisto[dirName][typeH].SetName(typeH+"_ExpQCD")
            dicMergedExpHisto[dirName][typeH].Write()
            # Rebin
            BinningTauPT = array.array('d', [0,40,42.5,45,50,55,60,75,90,110,120,500]) # 0,40,45,50,55,60,100,inf
            BinningTau2PT = array.array('d', [40,45,50,55,60,65,70,75,80,85,90,95,100,105,110,115,120]) # 0,40,45,50,55,60,100,inf
            BinningVisibleMass = array.array('d', [0,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,220,250,300])
            if "TaupT" in  inputTDir_fineBin:
                print "\tRebin Tau pT fine binning histogram ... " + typeH
                rebinnedHist.append(dicMergedExpHisto[dirName][typeH].Rebin(len(BinningTauPT)-1,"rebinned_"+typeH,BinningTauPT))
            elif "Tau2pT" in  inputTDir_fineBin:
                print "\tRebin Tau2 pT fine binning histogram ... " + typeH
                rebinnedHist.append(dicMergedExpHisto[dirName][typeH].Rebin(len(BinningTau2PT)-1,"rebinned_"+typeH,BinningTau2PT))
            elif "VisMass" in inputTDir_fineBin:
                print "\tRebin Visible Mass fine binning histogram ... " + typeH
                rebinnedHist.append(dicMergedExpHisto[dirName][typeH].Rebin(len(BinningVisibleMass)-1,"rebinned_"+typeH,BinningVisibleMass)) 
            rebinnedHist[-1].SetName(typeH+"_ExpQCD_Rebin")
            rebinnedHist[-1].Write()
        # Write FF_qcd = iso / ai
        print "\t:: Divide Expected QCD in TDirectory " + outputTDir_rawFakeFactor.GetName()
        HFF1 = outputTDir_rawFakeFactor.Get("isoDR1_ExpQCD_Rebin").Clone()
        HFF1.Divide(outputTDir_rawFakeFactor.Get("aiDR1_ExpQCD_Rebin").Clone())
        HFF2 = outputTDir_rawFakeFactor.Get("isoDR2_ExpQCD_Rebin").Clone()
        HFF2.Divide(outputTDir_rawFakeFactor.Get("aiDR2_ExpQCD_Rebin").Clone())
        
        HFF1.SetName("rawFakeFactor_1")
        HFF1.Write()
        HFF2.SetName("rawFakeFactor_2")
        HFF2.Write()

