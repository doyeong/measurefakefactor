#!/usr/bin/env python
import os, sys, re
from optparse import OptionParser
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2F
import measureRawFF
import fitter
import estimateFakeBkg_fuc

parser = OptionParser()
parser.add_option('--raw', '-r', action='store_true',
                  default=False, dest='measurRawFF',
                  help='measure raw fakefactor'
                  )
parser.add_option('--closure', '-c', action='store_true',
                  default=False, dest='measureNonClosureCorrection',
                  help='measure non closure correction'
                  )
parser.add_option('--lepton', '-l', action='store_true',
                  default=False, dest='measurLeptonIsolationCorrection',
                  help='measure lepton isolation correction'
                  )
parser.add_option('--OSextrapolation', '-o', action='store_true',
                 default=False, dest='measurOSextrapolation',
                  help='measure OS extrapolation correction'
                  )
(options, args) = parser.parse_args()

TauWP = "MVATight"#"DeepLoose"
ROOT.gROOT.SetBatch(ROOT.kTRUE)

def main():
    path="/hdfs/store/user/doyeong/Sliced/Sliced_2018X10_forFF/nominal/merged/"
    file_list = os.listdir(path)
    print ">> Start! "
    # Output File

    if options.measurRawFF is True:
        openOptionMeasurRawFF = 'recreate'
    else:
        openOptionMeasurRawFF = 'read'
    if options.measureNonClosureCorrection is True:
        openOptionMeasurClosure = 'recreate'
    else:
        openOptionMeasurClosure = 'read'
    if options.measurLeptonIsolationCorrection is True:
        openLeptonIso = 'recreate'
    else:
        openLeptonIso = 'read'
    if options.measurOSextrapolation is True:
        openOSextrapol = 'recreate'
    else:
        openOSextrapol = 'read'

    outputFileRaw = TFile( 'rawFakeFactor2018.root', openOptionMeasurRawFF )
    outputFileClosure = TFile( 'closureFakeFactor2018.root', openOptionMeasurClosure )    
    outputFileLeptonIsolation = TFile( 'lepIsoFakeFactor2018.root', openLeptonIso )
    outputFileOSextrapolation = TFile( 'osExtrapolFakeFactor2018.root', 'recreate' )

    # Measure Raw FF in DR
    if options.measurRawFF is True:
        print "\tMeasure Raw FF : 0jet"
        outputTDir_fineBinTaupTs_zeroJet = outputFileRaw.mkdir(TauWP+"_fineBinTaupTs_zeroJet")
        measureRawFF.storeFinBinningHistogram(file_list, TauWP, outputTDir_fineBinTaupTs_zeroJet, "zeroJet","tauPT","SS")
        outputTDir_rawFakeFactor_zeroJet = outputFileRaw.mkdir(TauWP+"_rawFakeFactor_zeroJet")
        measureRawFF.measureRawFF(outputFileRaw, outputTDir_rawFakeFactor_zeroJet,TauWP+"_fineBinTaupTs_zeroJet")
        print "\tFit Raw FF : 0jet"
        fitter.fitter(outputTDir_rawFakeFactor_zeroJet, "tauPT")
        print "\n\n\tMeasure Raw FF : multi-jet"
        outputTDir_fineBinTaupTs_multiJet = outputFileRaw.mkdir(TauWP+"_fineBinTaupTs_multiJet")
        measureRawFF.storeFinBinningHistogram(file_list, TauWP, outputTDir_fineBinTaupTs_multiJet, "multiJet","tauPT","SS")
        outputTDir_rawFakeFactor_multiJet = outputFileRaw.mkdir(TauWP+"_rawFakeFactor_multiJet")
        measureRawFF.measureRawFF(outputFileRaw, outputTDir_rawFakeFactor_multiJet, TauWP+"_fineBinTaupTs_multiJet")
        print "\tFit Raw FF : Multi-jet"
        outputTDir_rawFakeFactor_multiJet = outputFileRaw.Get(TauWP+"_rawFakeFactor_multiJet")
        fitter.fitter(outputTDir_rawFakeFactor_multiJet, "tauPT")


    if options.measureNonClosureCorrection is True:
        # The non-closure correction : Derived in SS region as a function of visible mass
        # Observed fake backgrounds - SS
        print "\n\n\tNon-closure correction : Observed fake backgrounds 0jet"
        outputTDir_fineBinVisMass_zeroJet = outputFileClosure.mkdir(TauWP+"_fineBinVisMass_zeroJet")
        measureRawFF.storeFinBinningHistogram(file_list, TauWP, outputTDir_fineBinVisMass_zeroJet, "zeroJet","vis_mass","SS")
        outputTDir_rawFakeFactor_VisMass_zeroJet = outputFileClosure.mkdir(TauWP+"_rawFakeFactor_VisMass_zeroJet")
        measureRawFF.measureRawFF(outputFileClosure, outputTDir_rawFakeFactor_VisMass_zeroJet,TauWP+"_fineBinVisMass_zeroJet")
        print "\n\n\tNon-closure correction : Observed fake backgrounds multi-jet"
        outputTDir_fineBinVisMass_multiJet = outputFileClosure.mkdir(TauWP+"_fineBinVisMass_multiJet")
        measureRawFF.storeFinBinningHistogram(file_list, TauWP, outputTDir_fineBinVisMass_multiJet, "multiJet","vis_mass","SS")
        outputTDir_rawFakeFactor_VisMass_multiJet = outputFileClosure.mkdir(TauWP+"_rawFakeFactor_VisMass_multiJet")
        measureRawFF.measureRawFF(outputFileClosure, outputTDir_rawFakeFactor_VisMass_multiJet,TauWP+"_fineBinVisMass_multiJet")

        # Estimated fake backgrounds
        print "\n\n\tNon-closure correction : Estimated fake backgrounds 0jet"
        estimateFakeBkg_fuc.storeFinBinningHistogram(TauWP, outputFileRaw, outputFileClosure, "vis_mass", "zeroJet", "SS")
        fitter.fitterClosure(outputTDir_rawFakeFactor_VisMass_zeroJet, "vis_mass")
        print "\n\n\tNon-closure correction : Estimated fake backgrounds multi-jet"
        estimateFakeBkg_fuc.storeFinBinningHistogram(TauWP, outputFileRaw, outputFileClosure, "vis_mass", "multiJet", "SS")
        fitter.fitterClosure(outputTDir_rawFakeFactor_VisMass_multiJet, "vis_mass")
        #print "\tFit Raw FF : VisMass"
        #fitter.fitter(outputTDir_rawFakeFactor_VisMass, "vis_mass")

    if options.measurLeptonIsolationCorrection is True:
        # t2_pt, SS measure, OS apply
        print "\n\n\tLepton isolation correction : Observed fake backgrounds 0jet"
        outputTDir_fineBinTau2pT_zeroJet = outputFileLeptonIsolation.mkdir(TauWP+"_fineBinTau2pT_zeroJet")
        measureRawFF.storeFinBinningHistogram(file_list, TauWP, outputTDir_fineBinTau2pT_zeroJet, "zeroJet","t2_pt","SS")
        outputTDir_rawFakeFactor_Tau2pT_zeroJet = outputFileLeptonIsolation.mkdir(TauWP+"_rawFakeFactor_Tau2pT_zeroJet")
        measureRawFF.measureRawFF(outputFileLeptonIsolation, outputTDir_rawFakeFactor_Tau2pT_zeroJet,TauWP+"_fineBinTau2pT_zeroJet")
        print "\n\n\tLepton isolation correction : Observed fake backgrounds multi-jet"
        outputTDir_fineBinTau2pT_multiJet = outputFileLeptonIsolation.mkdir(TauWP+"_fineBinTau2pT_multiJet")
        measureRawFF.storeFinBinningHistogram(file_list, TauWP, outputTDir_fineBinTau2pT_multiJet, "multiJet","t2_pt","SS")
        outputTDir_rawFakeFactor_Tau2pT_multiJet = outputFileLeptonIsolation.mkdir(TauWP+"_rawFakeFactor_Tau2pT_multiJet")
        measureRawFF.measureRawFF(outputFileLeptonIsolation, outputTDir_rawFakeFactor_Tau2pT_multiJet,TauWP+"_fineBinTau2pT_multiJet")

        # Estimated fake backgounds 
        print "\n\n\tLepton isolation correction : Apply non-closure correction 0jet"
        estimateFakeBkg_fuc.storeFinBinningHistogram(TauWP, outputFileClosure, outputFileLeptonIsolation, "t2_pt", "zeroJet", "SS")
        fitter.fitterLepIso(outputTDir_rawFakeFactor_Tau2pT_zeroJet, "t2_pt", "2")
        print "\n\n\tLepton isolation correction : Apply non-closure correction multi-jet"
        estimateFakeBkg_fuc.storeFinBinningHistogram(TauWP, outputFileClosure, outputFileLeptonIsolation, "t2_pt", "multiJet", "SS")
        fitter.fitterLepIso(outputTDir_rawFakeFactor_Tau2pT_multiJet, "t2_pt", "2")


    if options.measurOSextrapolation is True:
        # Observed fake backgrounds - OS, vis_mass - outputTDir_rawFakeFactor_VisMass_zeroJet
        print "\n\n\tOSextrapolation correction : Observed fake backgrounds 0jet"
        outputTDir_fineBinVisMass_zeroJet_OS = outputFileOSextrapolation.mkdir(TauWP+"_fineBinVisMass_zeroJet_OS")
        measureRawFF.storeFinBinningHistogram(file_list, TauWP, outputTDir_fineBinVisMass_zeroJet_OS, "zeroJet","vis_mass","OS")
        outputTDir_rawFakeFactor_VisMass_zeroJet_OS = outputFileOSextrapolation.mkdir(TauWP+"_rawFakeFactor_VisMass_zeroJet_OS")
        measureRawFF.measureRawFF(outputFileOSextrapolation, outputTDir_rawFakeFactor_VisMass_zeroJet_OS,TauWP+"_fineBinVisMass_zeroJet_OS")

        print "\n\n\tOSextrapolation correction : Observed fake backgrounds multi-jet"
        outputTDir_fineBinVisMass_multiJet_OS = outputFileOSextrapolation.mkdir(TauWP+"_fineBinVisMass_multiJet_OS")
        measureRawFF.storeFinBinningHistogram(file_list, TauWP, outputTDir_fineBinVisMass_multiJet_OS, "multiJet","vis_mass","OS")
        outputTDir_rawFakeFactor_VisMass_multiJet_OS = outputFileOSextrapolation.mkdir(TauWP+"_rawFakeFactor_VisMass_multiJet_OS")
        measureRawFF.measureRawFF(outputFileOSextrapolation, outputTDir_rawFakeFactor_VisMass_multiJet_OS,TauWP+"_fineBinVisMass_multiJet_OS")

        # Estimated fake backgounds - OS, vis_mass
        print "\n\n\tOSextrapolation correction : Estimated fake backgrounds 0jet"
        estimateFakeBkg_fuc.storeFinBinningHistogram(TauWP, outputFileRaw, outputFileOSextrapolation, "vis_mass", "zeroJet", "OS")
        fitter.fitterClosure(outputTDir_rawFakeFactor_VisMass_zeroJet_OS, "vis_mass")
        print "\n\n\tOSextrapolation correction : Estimated fake backgrounds multi-jet"
        estimateFakeBkg_fuc.storeFinBinningHistogram(TauWP, outputFileRaw, outputFileOSextrapolation, "vis_mass", "multiJet", "OS")
        fitter.fitterClosure(outputTDir_rawFakeFactor_VisMass_multiJet_OS, "vis_mass")

        # raw FF derived in AI SS region - outputTDir_rawFakeFactor_zeroJet
        # corrected as a function of vis mass
        #print "\n\n\tOS extrapolation correction : Estimated fake backgrounds 0jet"
        #measureRawFF.storeFinBinningHistogram(TauWP, outputFileRaw, outputFileOSextrapolation, "vis_mass", "zeroJet", "OS")
        
        #fitter.fitterClosure(outputTDir_rawFakeFactor_VisMass_zeroJet, "vis_mass")
        

    # Correction for OS/SS extrapolation outputFileLeptonIsolation
    '''
    print "\n\n\tMeasure Raw FF in AR2 SS"
    outputTDir_fineBinTaupTsOSSS = outputFile.mkdir(TauWP+"_fineBinTaupTsOSSS")
    measureRawFF.storeFinBinningHistogram(file_list, TauWP, outputTDir_fineBinTaupTsOSSS, "noJetSplitting","vis_mass")
    outputTDir_rawFakeFactorOSSS = outputFile.mkdir(TauWP+"_rawFakeFactorOSSS")
    measureRawFF.measureRawFF(outputFile, outputTDir_rawFakeFactorOSSS, TauWP+"_fineBinTaupTsOSSS")
    '''
    
    outputFileRaw.Close()
    outputFileClosure.Close()

if __name__ == "__main__":
    main()
