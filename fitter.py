import ROOT
from ROOT import TF1, TGraphAsymmErrors, TMath, TPad
import math

ROOT.gStyle.SetOptFit(1111)

def fitFunction(x, par0, par1):
    return par0 + par1 *(x-40)

def fitFunc_Exp3Par(x, par):
    return par[0] + par[1]* (x[0]-40)

def fitFunc_Line2Par2(x, par):
    return par[0] + par[1]* (x[0]-25)

def fitFunc_Landau(x, par):
    return par[0]*(ROOT.TMath.Landau((x[0]),par[1],par[2],0))# + x[0]+par[3] + par[4]

def fitFunc_Line2Par(x, par):
    return par[0] + par[1] * x[0]

def fitFunc_Expo4Par(x, par):
    return par[0] + par[1]*(ROOT.TMath.Exp(-par[2]*x[0]+par[3])) + x[0]+par[4]

def fitFunc_4Par(x, par):
    return par[0]+par[1]*x[0]+par[2]*x[0]*x[0]+par[3]*x[0]*x[0]*x[0]+par[4]*x[0]*x[0]*x[0]*x[0]+par[5]*x[0]*x[0]*x[0]*x[0]*x[0]


def dealNegativeBins(histo):
    numBins = histo.GetNbinsX()
    for iBin in range(0,numBins):
        if histo.GetBinContent(iBin)<0.0 :
            histo.SetBinContent(iBin,0)
            histo.SetBinError(iBin,0)
    
def makeBinsInteger(son, mom):
    numBins = son.GetNbinsX()
    dealNegativeBins(son)
    dealNegativeBins(mom)
    for iBin in range(0,numBins):
        summedBinContent = son.GetBinContent(iBin) + mom.GetBinContent(iBin)
        summedBinErrorSqure = son.GetBinError(iBin)*son.GetBinError(iBin) + mom.GetBinError(iBin)*mom.GetBinError(iBin)
        summedBinError = ROOT.TMath.Sqrt(summedBinErrorSqure)
        if summedBinContent>0.0 and summedBinError>0.0:
            nEff = (summedBinContent/summedBinError)*(summedBinContent/summedBinError)
            avWeight = summedBinContent/nEff
            binContentSon = round(son.GetBinContent(iBin)/avWeight)
            son.SetBinContent(iBin, binContentSon)
            son.SetBinError(iBin, ROOT.TMath.Sqrt(binContentSon))
            binContentMom = round(mom.GetBinContent(iBin)/avWeight)
            mom.SetBinContent(iBin, binContentMom)
            mom.SetBinError(iBin, ROOT.TMath.Sqrt(binContentMom))

def plotter(plotName, TGraph_FR, theFit, plotTitle, titleY, setLog):
    canvas = ROOT.TCanvas("canvas", "", 800, 800)
    if "setLog" in setLog:
        canvas.SetLogx()
    canvas.SetTitle("")
    canvas.SetGrid()
    if "Correction" not in titleY:
        TGraph_FR.GetYaxis().SetRangeUser(0.00,0.70)
    if "Correction" in titleY:
        print "\tMaking correction distribution"
        #TGraph_FR.GetYaxis().SetRangeUser(0.50,1.50)
    TGraph_FR.GetYaxis().SetTitle(titleY)
    TGraph_FR.GetXaxis().SetRangeUser(40,500)
    TGraph_FR.GetXaxis().SetTitle(plotTitle)#"#tau_{h,1} p_{T} [GeV]")
    TGraph_FR.SetTitle("")
    TGraph_FR.Draw("PAE")
    TGraph_FR.SetLineWidth(3)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextAlign(12)
    latex.SetTextSize(0.04)
    latex.DrawLatex(0.55, .96, "59.5 fb^{-1} (2018, 13 TeV)")
    if "dummy" not in theFit.GetTitle():
        theFit.SetLineColor(2)
        theFit.Draw("same")
    #theFit2.SetLineColor(8)
    #theFit2.Draw("same")
    canvas.SaveAs("FakeFactor/plots/"+plotName) #fit_"+TDir_rawFakeFactor.GetName()+"_"+leg+".pdf")


def sonAndMomSuperposer(son, mom, plotName):
    canvas = ROOT.TCanvas("sonAndmom", "", 800, 800)
    canvas.SetTitle("")
    padUp = TPad("pad1", "pad1", 0, 0.5, 1, 1.0)
    padUp.SetBottomMargin(0)
    padUp.Draw()
    padUp.cd()
    son.SetStats(0)
    son.SetMaximum(max(son.GetMaximum(),mom.GetMaximum())*1.30)
    mom.SetStats(0)
    son.Draw("histe")
    mom.SetLineColor(2)
    mom.Draw("histesame")
    legend = ROOT.TLegend(0.45, 0.70, 0.88, 0.85, "", "brNDC")
    legend.SetLineWidth(0)
    legend.AddEntry(son, "Expected [Numerator]", "lp")
    legend.AddEntry(mom, "Estimated [Denominator]", "lp")
    legend.Draw("same")

    canvas.cd()
    '''
    obsPave = ROOT.TPaveText(0.0, 0.05, 1.0, 0.10, "NDC")
    obsPave.SetBorderSize(   0 )
    obsPave.SetFillStyle(    0 )
    obsPave.SetTextAlign(   31 )
    obsPave.SetTextSize (  0.5 )
    obsPave.SetTextColor(    1 )
    obsPave.SetTextFont (   42 )
    obsPave.AddText(plotName)
    obsPave.Draw()
    '''

    padDown = TPad("pad2", "pad2", 0, 0.05, 1, 0.5)
    padDown.SetTopMargin(0)
    padDown.SetBottomMargin(0.2)
    padDown.Draw()
    padDown.cd()
    dividedHisto = son.Clone()
    dividedHisto.SetLineColor(1)
    dividedHisto.Sumw2()
    dividedHisto.SetTitle("")
    dividedHisto.Divide(mom.Clone())
    dividedHisto.SetStats(0)
    dividedHisto.SetMarkerStyle(21)
    dividedHisto.GetXaxis().SetTitle(plotName)
    if "DividingHistograms_rawFakefactor" in plotName:
        dividedHisto.SetMaximum(0.4)#dividedHisto.GetMaximum()*1.2)
        dividedHisto.SetMinimum(0.2)#dividedHisto.GetMinimum()*0.8)
    else:
        dividedHisto.SetMaximum(1.5)#dividedHisto.GetMaximum()*1.2)
        dividedHisto.SetMinimum(0.8)#dividedHisto.GetMinimum()*0.8)

    dividedHisto.Draw("ep")
    canvas.SaveAs("FakeFactor/plots/"+plotName)

def fitFormulaMaker(TDir_rawFakeFactor, son, mom, plotTitle, leg):
    TGraph_FR = TGraphAsymmErrors(26)
    TGraph_FR.Divide(son, mom, "pois");
    yg = TGraph_FR.GetY()
    for i in range(0,5):
        print "yg["+str(i)+"] = " + str(yg[i])

    fMin, fMax, nPar = 40, 110, 3
    theFit = TF1("TF_"+son.GetName() + "_" + mom.GetName(), fitFunc_Landau, fMin, fMax, nPar)
    #theFit = TF1("theFit", fitFunc_Landau, fMin, fMax, nPar)
    theFit.SetParameter(0, 1.0)
    theFit.SetParameter(1, 25.0)
    theFit.SetParameter(2, 3.0)
    #theFit.SetParameter(3, 0.00000001)
    #theFit.SetParameter(4, 0.3)

    #theFit2 = TF1("theFit2", fitFunc_Line2Par, 110, 120, 2)

    xAxisMax = 500
    TGraph_FR.Fit("theFit", "R0")
    #TGraph_FR.Fit("theFit2", "R0")
    plotName = "fit_"+TDir_rawFakeFactor.GetName()+"_"+leg+".pdf"
    plotter(plotName, TGraph_FR, theFit, plotTitle, "f_{#tau}", "setLog")
    TDir_rawFakeFactor.cd()
    TGraph_FR.SetName(son.GetName() + "_" + mom.GetName())
    TGraph_FR.Write()

    return theFit


def fitter(TDir_rawFakeFactor, parameter):
    TDir_rawFakeFactor.cd()

    FitRange = [30,100]
    title1, title2, title3 = "Visible Higgs Mass [GeV]", "Visible Higgs Mass [GeV]", "Visible Higgs Mass [GeV]"
    if parameter is "tauPT":
        title1, title2, title3 = "#tau_{h,1} p_{T} [GeV]", "#tau_{h,2} p_{T} [GeV]", "#tau_{h} p_{T} [GeV]"
        
    # FF = Niso / Nani-iso    (AN(2))
    Son1 = TDir_rawFakeFactor.Get("isoDR1_ExpQCD_Rebin").Clone()
    Mom1 = TDir_rawFakeFactor.Get("aiDR1_ExpQCD_Rebin").Clone()
    print "makeBinsInteger("+Son1.GetName()+","+Mom1.GetName()+")"
    makeBinsInteger(Son1, Mom1)
    fitFormulaMaker(TDir_rawFakeFactor, Son1, Mom1, title1, "1").Write()
    sonAndMomSuperposer(Son1, Mom1, "DividingHistograms_rawFakefactor_leg1"+TDir_rawFakeFactor.GetName()+".pdf")

    Son2 = TDir_rawFakeFactor.Get("isoDR2_ExpQCD_Rebin").Clone()
    Mom2 = TDir_rawFakeFactor.Get("aiDR2_ExpQCD_Rebin").Clone()
    print "makeBinsInteger("+Son2.GetName()+","+Mom2.GetName()+")"
    makeBinsInteger(Son2, Mom2)
    fitFormulaMaker(TDir_rawFakeFactor, Son2, Mom2, title2, "2").Write()
    sonAndMomSuperposer(Son2, Mom2, "DividingHistograms_rawFakefactor_leg2"+TDir_rawFakeFactor.GetName()+".pdf")

    Son = Son1.Clone()
    Son.Add(Son2, 1)
    Son.SetName("isoDR_ExpQCD_Rebin")
    Mom = Mom1.Clone()
    Mom.Add(Mom2, 1)
    Mom.SetName("aiDR_ExpQCD_Rebin")
    print "makeBinsInteger(summed iso, summed ai)"
    makeBinsInteger(Son, Mom)
    fitFormulaMaker(TDir_rawFakeFactor, Son, Mom, title3, "3").Write()    
    sonAndMomSuperposer(Son, Mom, "DividingHistograms_rawFakefactor_combinedLegs"+TDir_rawFakeFactor.GetName()+".pdf")

def fitterClosure(TDir, parameter):
    TDir.cd()
    title = parameter
    if parameter is "vis_mass":
        title = "Visible Higgs Mass [GeV]"
    elif parameter is "t2_pt":
        title = "#tau_{h,2} p_{T} [GeV]"

    # Corr = Exp./Est.
    Son = TDir.Get("isoDR1_ExpQCD_Rebin").Clone()
    Son.Write()
    Mom = TDir.Get("aiFF_Estimated_Rebin")
    print "makeBinsInteger("+Son.GetName()+","+Mom.GetName()+")"
    makeBinsInteger(Son, Mom)
    TGraph_FR = TGraphAsymmErrors(26)
    TGraph_FR.Divide(Son, Mom, "pois")
    TGraph_FR.SetName(Son.GetName() + "_" + Mom.GetName())
    TGraph_FR.Write()
    plotName = "fit_"+TDir.GetName()+".pdf"
    theFit = TF1("dummy", fitFunc_Landau, 0,1,1) # place holder
    yaxisName = "Non-closure Correction"
    if "OS" in TDir.GetName():
        yaxisName = "OS/SS Extrapolation Correction"
    plotter(plotName, TGraph_FR, theFit, title, yaxisName, "setXXLog")
    sonAndMomSuperposer(Son, Mom, "DividingHistograms_NonClosure_"+TDir.GetName()+".pdf")

def fitterLepIso(TDir, parameter, leg):
    TDir.cd()
    title = parameter
    if parameter is "vis_mass":
        title = "Visible Higgs Mass [GeV]"
    elif parameter is "t2_pt":
        title = "#tau_{h,2} p_{T} [GeV]"

    # Corr = Exp./Est.
    Son = TDir.Get("isoDR"+leg+"_ExpQCD_Rebin") 
    Mom = TDir.Get("aiFF_Estimated_Rebin")
    print "makeBinsInteger("+Son.GetName()+","+Mom.GetName()+")"
    makeBinsInteger(Son, Mom)
    TGraph_FR = TGraphAsymmErrors(26)
    TGraph_FR.Divide(Son, Mom, "pois")
    TGraph_FR.SetName(Son.GetName() + "_" + Mom.GetName())
    TGraph_FR.Write()
    plotName = "fit_"+TDir.GetName()+".pdf"
    theFit = TF1("dummy", fitFunc_Landau, 0,1,1) # place holder
    plotter(plotName, TGraph_FR, theFit, title, "P_{T, #tau} Correction", "setXXLog")
    sonAndMomSuperposer(Son, Mom, "DividingHistograms_IsoLepton_"+TDir.GetName()+".pdf")
