import DefineControlRegions
import array
import ROOT
from ROOT import TF1, TH1, TGraphAsymmErrors
from ROOT import gROOT

gROOT.Macro("pluginFakeFactor/get_raw_FF.C+")

def storeFinBinningHistogram(TauWP, outputFileRaw, outputFileClosure, parameter, njets, charge):
    # outputFileRaw - where FF is
    # outputFileClosure - apply FF and store estimated fake bkg. here
    isoRegion, aiRegion1, aiRegion2 = DefineControlRegions.derivingRegion(TauWP, charge, njets)
    extractBranch1 = parameter
    extractBranch2 = parameter
    fineBinning = "120,0,300"
    if parameter is "t2_pt":
        fineBinning = "160,40,120"
    elif parameter is "tauPT":
        extractBranch1 = "t1_pt"
        extractBranch2 = "t2_pt"
        fineBinning = "200,0,500"

    f=ROOT.TFile("/hdfs/store/user/doyeong/Sliced/Sliced_2018X10_forFF/nominal/merged/data_obs.root")
    tree = f.Get("tautau_tree")  
    processName = "jetFakes"
    print "\tExtracting histograms as a function of " +parameter+ " ... " + processName 
    
    rawFFZeroJet = outputFileRaw.Get(TauWP+"_rawFakeFactor_zeroJet/isoDR_ExpQCD_Rebin_aiDR_ExpQCD_Rebin")
    rawFFMultiJet = outputFileRaw.Get(TauWP+"_rawFakeFactor_multiJet/isoDR_ExpQCD_Rebin_aiDR_ExpQCD_Rebin")
    print "\tcd to "+outputFileClosure.GetName()
    outputFileClosure.cd()
    #tree.Draw(extractBranch1+">>ai1FFApplied("+fineBinning+")", "getRawZeroJetFF(t1_pt)*aiRegion1 + getRawMultiJetFF(t2_pt)*aiRegion2") 
    #weight = "(0.5*(%s*%s)+0.5*(%s*%s))" % ("getRawZeroJetFF(t1_pt)",aiRegion1,"getRawMultiJetFF(t2_pt)",aiRegion2)
    weight = "1.0"

    # Applying raw fake factor
    if njets is "zeroJet":
        weight = "(0.5*(%s*%s)+0.5*(%s*%s))" % ("getRawZeroJetFF(t1_pt)",aiRegion1,"getRawZeroJetFF(t2_pt)",aiRegion2)
    elif njets is "multiJet":
        weight = "(0.5*(%s*%s)+0.5*(%s*%s))" % ("getRawMultiJetFF(t1_pt)",aiRegion1,"getRawMultiJetFF(t2_pt)",aiRegion2)

    print "weight : "+weight
    tree.Draw(extractBranch1+">>aiFF_Estimated("+fineBinning+")", weight)
    tree.Draw(weight+">>aiFF_EstimatedWeight")

    #"ROOT.get_raw_FF(t1_pt,rawFFZeroJet)")
    #tree.Draw(extractBranch1+">>ai1FFApplied("+fineBinning+")", "getRawFF(t1_pt)")

    #"(%s && (%s))"%(aiRegion1, "(njets==0)*get_raw_FF(t1_pt,rawFFZeroJet)+(njets!=0)*get_raw_FF(t1_pt,rawFFMultiJet)"))

    H_estimatedFakes = outputFileClosure.Get("aiFF_Estimated")
    H_estimatedFakesW = outputFileClosure.Get("aiFF_EstimatedWeight")
    parameterInDirName = "VisMass"
    if "t2_pt" is parameter:
        parameterInDirName = "Tau2pT"
    outputDirName = TauWP+"_rawFakeFactor_"+parameterInDirName+"_"+njets
    if charge is "OS":
        outputDirName+="_OS"
    dir = outputFileClosure.Get(outputDirName)#TauWP+"_rawFakeFactor_"+parameterInDirName+"_"+njets)
    dir.cd()
    H_estimatedFakes.Write()
    H_estimatedFakesW.Write()
    # Store rebined histogram
    BinningVisibleMass = array.array('d', [0,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,220,250,300])
    BinningTau2PT = array.array('d', [40,45,50,55,60,65,70,75,80,85,90,95,100,105,110,115,120])

    print "\tRebin Visible Mass fine binning histogram ... " 
    if "vis_mass" is parameter:
        H_estimatedFakes_Rebin = H_estimatedFakes.Rebin(len(BinningVisibleMass)-1,"rebinned_"+H_estimatedFakes.GetName(),BinningVisibleMass).Clone()
        H_estimatedFakes_Rebin.SetName("aiFF_Estimated_Rebin")
        H_estimatedFakes_Rebin.Write()
    elif "t2_pt" is parameter:
        H_estimatedFakes_Rebin = H_estimatedFakes.Rebin(len(BinningTau2PT)-1,"rebinned_"+H_estimatedFakes.GetName(),BinningTau2PT).Clone()
        H_estimatedFakes_Rebin.SetName("aiFF_Estimated_Rebin")
        H_estimatedFakes_Rebin.Write()

